export function sum(...args: number[]): number {
    if (!args.length) {
        return 0;
    }

    if (args.length === 1) {
        return args.shift();
    }

    return args.reduce((f, s) => f + s);
}
