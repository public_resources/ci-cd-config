import { sum } from '../src/sum';

describe('Sum test', () => {
    it('Test without arguments', () => {
        expect(sum()).toBe(0);
    });

    it('Test with one argument', () => {
        expect(sum(5)).toBe(5);
    });

    it('Test with two arguments', () => {
        expect(sum(4, 8)).toBe(12);
    });

    it('Test with many arguments', () => {
        expect(sum(2, 5, 10, 3)).toBe(20);
    });
});
